
# fedbox

fedbox is a vagrant and virtualbox setup for running a fedora with full ui under windows.

All setup is done from scratch, and I'll try to document everything here. The fedbox is ready for ssh-ing into softlayer and pulling docker images / librarys from the tss mirror of softlayer.

#### Make sure you have the following versions installed, I've tested with them:
* Virtualbox version: 5.1.22
* Vagrant version: 1.9.7.

#### To Use this box:
* clone this repository
* modify shell script in Vagrantfile to include YOUR user data
* copy your secrets into the same directory, e.g.
** id_rsa
** id_rsa_sl_cn
** cntlm.conf
* get the current box from me (currently available at: 'P:\_Projects\smartservices\fed26') and name it fedbox.box
* run `vagrant up` which will run provisioning script also included in Vagrantfile
* wait with login until vagrant up command is complete
* on login screen, click gear-wheel to select awesome if you want to.


### How the box was setup from scratch

* installed fedora workstation, default settings, german keyboard, vagrant user with vagrant password
* see https://download.fedoraproject.org/pub/fedora/linux/releases/25/Workstation/x86_64/iso/Fedora-Workstation-netinst-x86_64-25-1.3.iso
* reset pwd if needed `passwd -f`
* install kernel sources for building guest additions

>     sudo dnf install kernel-devel-$(uname -r)

* insert guest additions from virtualbox menu

>     cd /run/media/vagrant/VBOXADDITIONS*/
>     sudo ./VBoxLinuxAdditions.sh

* edit /etc/default/grub, change lines

>     GRUB_TIMEOUT=0
>     GRUB_HIDDEN_TIMEOUT=0
>     GRUB_HIDDEN_TIMEOUT_QUIET=true

* back in terminal, enter

>     sudo grub2-mkconfig -o /boot/grub2/grub.cfg

* ssh setup and vagrant user: put public key in here `~/.ssh/authorized_keys`, see https://www.vagrantup.com/docs/boxes/base.html https://github.com/mitchellh/vagrant/tree/master/keys
* create file /etc/sudoers.d/vagrant with content

>     vagrant ALL=(ALL) NOPASSWD: ALL

* install and setup docker

>     sudo dnf install docker
>     sudo systemctl enable docker

* redirect dockerhub moovel to tss proxy

>     vi /etc/hosts
>     # add line
>     53.48.21.117 dockerhub.moovel.own

* edit docker config under /etc/sysconfig/docker, make sure it includes:

>	    HTTP_PROXY=http://127.0.0.1:3128/
>	    NO_PROXY='localhost,127.0.0.1,.detss.corpintra.net,.moovel.own,10.0.2.15'
>	    INSECURE_REGISTRY='--insecure-registry s415vmmt449.detss.corpintra.net:443 --insecure-registry dockerhub.moovel.own:443'
>	    ADD_REGISTRY='--add-registry s415vmmt449.detss.corpintra.net:443'

* enable deprecated sha1 for softlayer, see https://unix.stackexchange.com/questions/340844/how-to-enable-diffie-hellman-group1-sha1-key-exchange-on-debian-8-0/340853 and enable ssh-dss keys

>	    # in file /etc/ssh/ssh_config
>	    Host *
>	        KexAlgorithms +diffie-hellman-group1-sha1
>	        PubkeyAcceptedKeyTypes=+ssh-dss
    
* install java and maven (includes java dev tools)

>	    sudo dnf install java-1.8.0-openjdk
>           sudo dnf install maven

* in .bashrc `export JAVA_HOME=/usr/lib/java-1.8.0-openjdk`

* make logfile writable

>     sudo chmod ugo+w /var/log

* install chrome - download rpm - then run `sudo dnf install [chrome].rpm`

* install awesome

>     sudo dnf install awesome

* install cntlm

>     sudo dnf install cntlm -y
>     sudo systemctl enable cntlm
>     sudo systemctl start cntlm

* set http proxy - create file /etc/profile.d/proxy with content

>     export http_proxy=http://127.0.0.1:3128
>     export https_proxy=http://127.0.0.1:3128

* set proxy for dnf to install packages, see https://www.cyberciti.biz/faq/how-to-use-dnf-command-with-a-proxy-server-on-fedora/

>     sudo vi /etc/dnf/dnf.conf
>     # add line
>     proxy=http://localhost:3128

## That's it! Easy huh ;-)
* now simply package the box

>	    vagrant package --base <baseboxnameinvirtualbox> --output <outputfile.box>